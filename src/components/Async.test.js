import { render, screen } from "@testing-library/react";
import Async from "./Async";

describe("Async Component", () => {
  test('Render "listitem" ', async () => {
    render(<Async />);

    const listItemElement = await screen.findAllByRole(
      "listitem",
      {},
      { timeout: 500 }
    );
    expect(listItemElement).not.toHaveLength(0);
  });

  test("Simulate data ", async () => {
    window.fetch = jest.fn();

    window.fetch.mockResolvedValueOnce({
      json: async () => [{ id: "p1", title: "First post " }],
    });

    render(<Async />);

    const listItemElement = await screen.findAllByRole("listitem");
    expect(listItemElement).not.toHaveLength(0);

    const postElement = await screen.findByText("First post helllo");
    expect(postElement).toBeInTheDocument();
  });
});
