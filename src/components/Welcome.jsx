import React, { useState } from "react";

const Welcome = () => {
  const [visible, setVisible] = useState(false);

  return (
    <div>
      <div>Hello welcome to Unit Testing Session</div>
      {!visible && <p>Button is Not Clicked</p>}
      {visible && <p>Button is Clicked</p>}
      <button
        onClick={() => {
          setVisible(true);
        }}
        data-testid="changeTextButton"
      >
        Change Text
      </button>
    </div>
  );
};

export default Welcome;
