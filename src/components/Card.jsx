import React from "react";

const Card = ({ title, imageURL, description }) => {
  return (
    <div>
      <h4 data-testid="title">{title}</h4>
      <img data-testid="image" src={imageURL} />
      <p data-testid="description">{description}</p>
      <input data-testid="input" placeholder="Input Element" />
    </div>
  );
};

export default Card;
