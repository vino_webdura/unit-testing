import { render, screen } from "@testing-library/react";
import Card from "./Card";

describe("Card Component", () => {
  test('Render "Card"', () => {
    render(
      <Card
        title="Hello from title"
        description="This is description"
        imageURL="https://gooleimage.com"
      />
    );

    let titleElement = screen.getByTestId("title");
    expect(titleElement).toHaveTextContent("Hello from title");

    let descElement = screen.getByTestId("description");
    expect(descElement).toHaveTextContent("This is description");

    let imageElement = screen.getByTestId("image");
    expect(imageElement).toHaveAttribute("src", "https://gooleimage.com");
  });

  test('Render "Input Element"', () => {
    render(
      <Card
        title="Hello from title"
        description="This is description"
        imageURL="https://gooleimage.com"
      />
    );

    let inputElement = screen.getByTestId("input");
    expect(inputElement).toHaveAttribute("placeholder", "Input Element");
  });
});
