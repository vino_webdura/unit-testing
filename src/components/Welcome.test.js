import { render, screen } from "@testing-library/react";
import Welcome from "./Welcome";
import userEvents from "@testing-library/user-event";

test("Testing Welcome Component", () => {
  // Arrange
  render(<Welcome />);

  //Assert
  const welcomeTextElement = screen.getByText(
    "Hello welcome to Unit Testing Session",
    { exact: false }
  );
  expect(welcomeTextElement).toBeInTheDocument();
});

describe("Button Click", () => {
  test('Render "Button is Not Clicked" when button is not clicked', () => {
    render(<Welcome />);

    const notClickedElement = screen.getByText("Button is Not Clicked");

    expect(notClickedElement).toBeInTheDocument();
  });

  test('Render "Button is Clicked" when button is clicked', () => {
    render(<Welcome />);

    //Act
    const buttonElement = screen.getByTestId("changeTextButton");
    userEvents.click(buttonElement);

    const clickedElement = screen.getByText("Button is Clicked", {
      exact: true,
    });

    expect(clickedElement).toBeInTheDocument();
  });
});
